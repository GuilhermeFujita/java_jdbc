package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import model.Cliente;

public class ClienteDAO {

    private DataSource dataSource;

    public ClienteDAO(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public ArrayList<Cliente> readAll() {
        try {
            String SQL = "SELECT * FROM clientes";
            PreparedStatement ps = dataSource.getConnection().prepareStatement(SQL);
            ResultSet rs = ps.executeQuery();

            ArrayList<Cliente> lista = new ArrayList<Cliente>();
            while (rs.next()) {
                Cliente cli = new Cliente();
                cli.setId(rs.getInt("id"));
                cli.setNome(rs.getString("nome"));
                cli.setEmail(rs.getString("email"));
                cli.setTelefone(rs.getString("telefone"));
                lista.add(cli);
            }
            ps.close();
            return lista;

        } catch (SQLException ex) {

            System.err.println("Erro geral " + ex.getMessage());

        } catch (Exception ey) {

            System.err.println("Erro ao recuperar " + ey.getMessage());
        }
        return null;
    }
    
    public void inserirDados(String nome, String email, String telefone){
        try{
            String SQL = "INSERT INTO clientes (nome,email, telefone)"
                    + "values('"+nome+"', '"+email+"', '"+telefone+"')";
            PreparedStatement ps = dataSource.getConnection().prepareStatement(SQL);
            ps.executeUpdate(SQL);
            JOptionPane.showMessageDialog(null, "Dados inseridos com sucesso");
            ps.close();
        } catch(SQLException ex){
            JOptionPane.showMessageDialog(null, "Erro ao inserir os dados \n" + ex.getMessage());
        }
    }

    public void remover(int id) {
        try{
            String SQL = "DELETE FROM clientes WHERE id =" + id;
            PreparedStatement ps = dataSource.getConnection().prepareStatement(SQL);
            ps.executeUpdate(SQL);
            JOptionPane.showMessageDialog(null, "Cliente removido com sucesso");
            ps.close();
        } catch(SQLException ex){
            JOptionPane.showMessageDialog(null, "Erro ao remover " + ex.getMessage());
        }
    }
}